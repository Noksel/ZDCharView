Advanced qml ChartView and LineSeries.
AutoScale, Rectangle Zoom, Mouse Wheel Zoom, chart dragging.

Update graph data by Timeout. synchronous approach:

    Timer{
        id: mT
        interval: 300
        running: true
        repeat: true
        onTriggered: {
            someCppObject.update(upDt);
        }
    }


Or asyn through signal.

        AdvLineSeries{
            name: "Dynamic Data"
            objectName: "objLn"
            id: upDt
            Component.onCompleted: DataSource.resp.connect(upDt.append)
        }
