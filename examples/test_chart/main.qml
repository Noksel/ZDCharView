import QtQuick 2.15
import QtQuick.Controls 2.0
import QtQuick.Window 2.15
import QtQuick.Layouts 1.12
import QtCharts 2.3

import "qrc:/../../src"





ApplicationWindow  {

    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    Column{
        id: ctrlP
        spacing: 10
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.leftMargin: 10
        Button {
            text: "reScale"
            onPressed: {
                zdchv.reScale();
            }
        }
        CheckBox{
            id: chAutoS
            checked: true;
            text: "autoScale"
        }

    }

    Timer{
        id: mT
        interval: 300
        running: true
        repeat: true
        onTriggered: {
            mDs.update(upDt);
        }
    }

    ZDChartView{
        id: zdchv
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: ctrlP.right
        height: parent.height
        autoScale: chAutoS.checked

//        ValueAxis {
//            id: axisY
//            min: -0.5
//            max: 4
//        }

//        ValueAxis {
//            id: axisX
//            min: 0
//            max: 5
//        }

        AdvLineSeries{
            name: "Static Data"
            id: myLine
//            axisX: axisX
//            axisY: axisY
            XYPoint { x: 0; y: 0 }
            XYPoint { x: 1.1; y: 2.1 }
            XYPoint { x: 1.9; y: 3.3 }
            XYPoint { x: 2.1; y: 2.1 }
            XYPoint { x: 2.9; y: 4.9 }
            XYPoint { x: 3.4; y: 3.0 }
            XYPoint { x: 4.1; y: 3.3 }

        }

        AdvLineSeries{
            name: "Dynamic Data"
            id: upDt
//            axisX: axisX
//            axisY: axisY
        }
    }
}
