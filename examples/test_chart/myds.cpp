#include "myds.h"

myDS::myDS(QObject *parent) : QObject(parent),m_i(0.0)
{
    qRegisterMetaType <QAbstractSeries*>();

}

void myDS::update(QAbstractSeries *ser)
{
    if (ser) {
        QXYSeries *xySeries = static_cast<QXYSeries *>(ser);
        double y = sin(M_PI * m_i/10.0)*4+2;

        double x = m_i/10.0;
        xySeries->append(x,y);
        ++m_i;
    }
}
