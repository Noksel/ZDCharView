#ifndef MYDS_H
#define MYDS_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
#include <QtCharts/QXYSeries>
#include <math.h>


QT_CHARTS_USE_NAMESPACE

class myDS : public QObject
{
    Q_OBJECT

private:
    double m_i;

public:
    explicit myDS(QObject *parent = nullptr);

public slots:
    void update (QAbstractSeries* ser);

signals:

};

#endif // MYDS_H
