#include <QtWidgets/QApplication>
#include <QQmlApplicationEngine>
#include "myds_emit.h"
#include <QQmlContext>

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    myDS_Emit mDs;
    engine.rootContext()->setContextProperty("mDs",&mDs);
    engine.load(url);

    //не рекомендуется!
    //    QTimer tmr;
    //
    //    qDebug()<< engine.rootObjects().first()->findChild<QObject*>("zdchv")->children();
    //    qDebug()<< engine.rootObjects().first()->findChild<QObject*>("objLn");



    //  QObject::connect(&tmr, &QTimer::timeout,&mDs,&myDS_Emit::getData);
    // QObject::connect(&mDs, SIGNAL(resp(QVariant,QVariant)),lineSer, SLOT(append(QVariant,QVariant)));

    //  tmr.start(300);


    // QObject::connect(&mDs,      SIGNAL(finishedGatheringDataForItem(QString)),
    //          lineSer, SLOT(updateViewWithItem(QString)));

    return app.exec();
}
