import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Window 2.0
import QtQuick.Layouts 1.12
import QtCharts 2.3
import "qrc:/../../src"

ApplicationWindow {
    visible: true
    width: 640
    height: 480
    title: qsTr("Hello World")

    ZDChartView{
        id: zdchv
      //  objectName: "zdchv"
        anchors.fill: parent
        autoScale: true

        AdvLineSeries{
            name: "Dynamic Data"
       //     objectName: "objLn"
            id: upDt
         //  Component.onCompleted: mDs.resp.connect(upDt.append)
        }
        Connections{
            target: mDs
            function onResp(x,y) {upDt.append(x,y)}

        }
        AdvLineSeries{
            name: "Static Data"
          //  objectName: "fchld"
            id: myLine
            XYPoint { x: 0; y: 0 }
            XYPoint { x: 1.1; y: 2.1 }
            XYPoint { x: 1.9; y: 3.3 }
            XYPoint { x: 2.1; y: 2.1 }
            XYPoint { x: 2.9; y: 4.9 }
            XYPoint { x: 3.4; y: 3.0 }
            XYPoint { x: 4.1; y: 3.3 }

        }

        Timer{
            id: mT
            interval: 300
            running: true
            repeat: true
            onTriggered: {
                mDs.getData();
            }
        }
    }
}
