#include "myds_emit.h"

myDS_Emit::myDS_Emit(QObject *parent) : QObject(parent),m_i(0.0)
{

}

void myDS_Emit::getData()
{
    double y = sin(M_PI * m_i/10.0)*4+2;
    double x = m_i/10.0;
    emit resp(x,y);
    ++m_i;
}

//void myDS_Emit::update(QAbstractSeries *ser)
//{
//    if (ser) {
//        QXYSeries *xySeries = static_cast<QXYSeries *>(ser);
//        double y = sin(M_PI * m_i/10.0)*4+2;

//        double x = m_i/10.0;
//        xySeries->append(x,y);
//        ++m_i;
//    }
//}
