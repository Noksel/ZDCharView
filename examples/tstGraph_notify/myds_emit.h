#ifndef MYDS_H
#define MYDS_H

#include <QObject>
#include <QtCharts/QAbstractSeries>
#include <QtCharts/QXYSeries>
#include <math.h>
#include <QDebug>


QT_CHARTS_USE_NAMESPACE

class myDS_Emit : public QObject
{
    Q_OBJECT

private:
    double m_i;

public:
    explicit myDS_Emit(QObject *parent = nullptr);

public slots:
  //  void update (QAbstractSeries* ser);
    void getData();

signals:
        void resp(double X, double Y);

};

#endif // MYDS_H
