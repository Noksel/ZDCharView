import QtQuick 2.0
import QtCharts 2.3


/*!
  наверное не надо тут хранить минимум и максимум, а просто проксировать сигнал дальше.
с XY а уже родитель будет получать координаты и сравнивать с текущим окном просмотра
  */
LineSeries{

    id: myLine
    signal newPoint(point nPoint)
    onPointAdded: {
        var point = this.at(index);
        newPoint(point)
        //    console.log("pointXY add",point.x,point.y);
        //    console.log("maxXY",maxX,maxY,"minXY",minX,minY);

    }
}
