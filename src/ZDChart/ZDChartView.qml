import QtQuick 2.0
import QtCharts 2.3

/**
  /TODO autoScale(Boolean) property
  */


ChartView{
    id: chv

    property double axMaxX
    property double axMinX
    property double axMaxY
    property double axMinY

    property int xScaleZoom: 0
    property int yScaleZoom: 0

    property int oldX: 0
    property int oldY: 0

    property bool autoScale: false

    Rectangle{
        id: recZoom
        border.color: "steelblue"
        border.width: 1
        color: "steelblue"
        opacity: 0.3
        visible: false
        transform: Scale { origin.x: 0; origin.y: 0; xScale: chv.xScaleZoom; yScale: chv.yScaleZoom}
    }

    /*!

      */
    /// !!! FILL PLOTT AREA !!!!
    //        Rectangle{
    //            anchors.fill: chv
    //            anchors.leftMargin: chv.plotArea.x
    //            anchors.topMargin: chv.plotArea.y
    //            anchors.rightMargin: chv.width-chv.plotArea.x-chv.plotArea.width
    //            anchors.bottomMargin: chv.height-chv.plotArea.y-chv.plotArea.height

    //            border.color: "steelblue"
    //            border.width: 1
    //            color: "steelblue"
    //            visible: true

    //        }

    MouseArea {
        id: maId
        anchors.fill: chv

        property bool shift: false

        // FILL ONLY PLOTT AREA OF ChartView
        //            anchors.fill: chv
        //            anchors.leftMargin: chv.plotArea.x
        //            anchors.topMargin: chv.plotArea.y
        //            anchors.rightMargin: chv.width-chv.plotArea.x-chv.plotArea.width
        //            anchors.bottomMargin: chv.height-chv.plotArea.y-chv.plotArea.height

        hoverEnabled: true  ///< CHECK!!- mouse left button is pressed or tobe switched off. overwise changeXY for mouse calling every time your move, wich out pressed
        onPressed: {
            maId.forceActiveFocus();
            if(shift===true){
                recZoom.x = mouseX;
                recZoom.y = mouseY;
                recZoom.visible = true;
            }

            else{
                // save old mouse XY for move.
                chv.oldX=mouseX;
                chv.oldY=mouseY;
            }
        }
        onMouseXChanged: {
            if (shift===true){
                if (mouseX - recZoom.x >= 0) {
                    chv.xScaleZoom = 1;

                    // show rectangeZOOM only in PlotArea right border
                    if (mouseX > chv.plotArea.x+chv.plotArea.width)
                        recZoom.width = chv.plotArea.x+chv.plotArea.width - recZoom.x;
                    else
                        recZoom.width = mouseX - recZoom.x;
                }
                else {
                    chv.xScaleZoom = -1;
                    // show rectangeZOOM only in PlotArea left border
                    if (mouseX<chv.plotArea.x)
                        recZoom.width = recZoom.x - chv.plotArea.x;
                    else
                        recZoom.width = recZoom.x - mouseX;
                }
            }
            else{
                if (mouse.buttons===Qt.LeftButton){
                    chv.scrollLeft(mouseX-chv.oldX);
                    chv.oldX=mouseX;
                }
            }
        }
        onMouseYChanged: {
            if (shift===true){
                if (mouseY - recZoom.y >= 0) {
                    chv.yScaleZoom = 1;
                    // show rectangeZOOM only in PlotArea top border
                    if (mouseY > chv.plotArea.y+chv.plotArea.height)
                        recZoom.height = chv.plotArea.y+chv.plotArea.height - recZoom.y;
                    else
                        recZoom.height = mouseY - recZoom.y;
                }
                else {
                    chv.yScaleZoom = -1;
                    // show rectangeZOOM only in PlotArea bottom border
                    if (mouseY<chv.plotArea.y)
                        recZoom.height = recZoom.y - chv.plotArea.y;
                    else
                        recZoom.height = recZoom.y - mouseY;
                    //recZoom.height = recZoom.y - mouseY;
                }
            }
            else {
                if (mouse.buttons===Qt.LeftButton){
                    chv.scrollUp(mouseY-chv.oldY);
                    chv.oldY=mouseY;
                }
            }
        }
        onReleased: {
            if(shift===true){
                var x = (mouseX >= recZoom.x) ? recZoom.x : mouseX
                var y = (mouseY >= recZoom.y) ? recZoom.y : mouseY
                chv.zoomIn(Qt.rect(x, y, recZoom.width, recZoom.height));
                recZoom.visible = false;
            }
        }

        focus: true
        onEntered: {
            maId.forceActiveFocus();
        }

        Keys.onPressed: {
            if (event.modifiers & Qt.ShiftModifier){
                shift=true;
                //console.log("sh_p");
            }
        }
        Keys.onReleased: {
            if (!(event.modifiers & Qt.ShiftModifier)){
                shift=false;
                recZoom.visible=false;
                //console.log("sh_r");
            }
        }

        onWheel: {
            //   console.log("W A",wheel.angleDelta.y/120)
            // ZOOM method 1
            if (wheel.angleDelta.y/120>0)
                chv.zoomIn();
            else
                chv.zoomOut();
            // ZOOM method 2. wheel.angleDelta.y/120== 1(-1)
            //chv.zoom(1+wheel.angleDelta.y/120*0.1)
        }

    }
    onSeriesAdded: {
        //   console.log("ADD SERIES");
        series.newPoint.connect(onNewPoint);
        for(var i=0;i<series.count;++i)
        {
            onNewPoint(series.at(i));
        }
    }

    function reScale(){
        if(chv.axisX())
            chv.axisX().max=axMaxX;
        if(chv.axisX())
            chv.axisX().min=axMinX;
        if(chv.axisY())
            chv.axisY().max=axMaxY;
        if(chv.axisY())
            chv.axisY().min=axMinY;

    }

    function onNewPoint(nPoint){

        if (nPoint.x>axMaxX)
            axMaxX=nPoint.x;
        else if (nPoint.x<axMinX)
            axMinX=nPoint.x;

        if (nPoint.y>axMaxY)
            axMaxY=nPoint.y;
        else if (nPoint.y<axMinY)
            axMinY=nPoint.y;
        if (autoScale)
            reScale();
    }
}
